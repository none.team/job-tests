""" Вводится число N — размер двумерной квадратной целочисленной матрицы и число K,
    затем построчно все элементы этой матрицы. Нужно отсортировать каждый столбец матрицы
    по близости элементов к числу K (сначала самые близкие к K). Вывести полученную матрицу.
    Примечание: при одинаковой близости сначала должен располагаться меньший элемент."""

import random

N = int(input())
K = int(input())

matrix = [[random.randint(0, 100) for _ in range(N)] for _ in range(N)]
print(matrix)
for i, mtx in enumerate(matrix):
    mtx.sort(key=lambda r: abs(K-r))
    print(mtx)

print(matrix)