import os.path

class Config(object):
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.abspath("app.sqlite"))
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    HOST = "https://m.avito.ru"
