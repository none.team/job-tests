from app import db


class Apartment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_ = db.Column(db.Integer, unique=True, nullable=False)
    coords = db.Column(db.JSON)
    url = db.Column(db.String, nullable=False)
    price = db.Column(db.Float, nullable=False, default=0)
    floor = db.Column(db.Integer)
    floors = db.Column(db.Integer)
    building = db.Column(db.Integer, default=1)
    rooms = db.Column(db.Integer, default=1)
    area = db.Column(db.Float)
    area_living = db.Column(db.Float)
    area_kitchen = db.Column(db.Float)
    address = db.Column(db.String)
    description = db.Column(db.Text)
    status = db.Column(db.Integer, nullable=False, default=0)
