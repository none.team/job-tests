var app = new Vue({
    el: '#app',
    data: {
        page: 1,
        sort: 'default',
        content: {
            min: null,
            max: null,
            mean: null,
        },
        disableForm: false,
        sendform: function () {
            this.disableForm = true;
            data = {
                'index': this.page,
                'sort': this.sort
            }
            axios.post('/v1/avito/items/price', data)
                .then(response => {
                   this.content = response.data;
                   this.disableForm = false;
                }).catch((err) => {
                  console.log(err)
                });
        }

    }
})