from wtforms import Form as Form_, IntegerField, StringField, validators
import requests, bs4, re
import pandas as pd


from app import models, db, Config


class Form(Form_):
    def load(self, params):
        if not isinstance(params, dict):
            return False
        for key in filter(lambda k: hasattr(self, k), params.keys()):
            try:                self[key].data = int(params[key])
            except ValueError:  self[key].data = params[key]
        return 'key' in locals()

class ApartmentsPriceForm(Form):
    index = IntegerField("Page Index", [
        validators.required(),
        validators.NumberRange(min=1)
    ])

    sort = StringField("Sort By", [
        validators.required()
    ])

    URL_SECRET = None
    URL_LOCATION = 637640
    URL_CATEGORY = 24

    PARAM_ROOM = 5696
    PARAM_BUILDING = 5245

    ANCHOR = b"(window.webpackJsonp=window.webpackJsonp||[]).push([[5]"

    FILE_TMP = "/tmp/avito.secret"

    def validate_sort(self, field):
        if field.data not in ["default", "date", "priceDesc", "priceAsc"]:
            raise validators.ValidationError('Not valid value.')

    def update_secret(self):
        request = requests.get(Config.HOST)
        soup = bs4.BeautifulSoup(request.content, 'html.parser')
        for script in filter(lambda src: "avito" in src.get('src'), soup.select("script[src]")):
            request = requests.get(script.get('src'), proxies={"http": "http://41.58.162.46:32696"})
            if self.ANCHOR in request.content:
                with open(self.FILE_TMP, "w") as file:
                    self.URL_SECRET = re.findall("[a-z0-9]{40,}", "{}".format(request.content))[0]
                    file.write(self.URL_SECRET)

    def as_data_frame(self, _):
        items = []
        for item in _.get("result", {}).get("items", None) if not _ is None else []:
            try:    items.append(item.get("value", {}).get("list")[0].get("value"))
            except: items.append(item.get("value"))
        df = pd.DataFrame(items)
        if not "price" in df.columns:
            df["price"] = pd.Series()
        df["price"] = df["price"]\
            .fillna("0")\
            .apply(lambda v: re.sub(r"\D", "", f"{v}"))\
            .astype(int)
        return df

    def get_apartments(self):
        try:    self.URL_SECRET = open(self.FILE_TMP)
        except: pass
        request = requests.get("{}/api/9/items".format(Config.HOST), params={
            "key": self.URL_SECRET,
            "locationId": self.URL_LOCATION,
            "categoryId": self.URL_CATEGORY,
            "page": self.index.data,
            "sort": self.sort.data,
            "params[549]": self.PARAM_ROOM,
            "params[498]": self.PARAM_BUILDING
        })
        if not request.status_code == 200:
            self.update_secret()
            return self.get_apartments()
        return self.as_data_frame(request.json())

class ApartmentCreateForm(Form):
    id_ = IntegerField("Id", [
        validators.required(),
    ])

    price = IntegerField("Price")

    url = StringField("Url", [
        validators.required()
    ])

    def validate_price(self, field):
        if not isinstance(field.data, int):
            raise validators.ValidationError('Wrong value for price.')

    def validate_id_(self, field):
        if db.session.query(models.Apartment.query.filter_by(id_=field.data).exists()).scalar():
            raise validators.ValidationError('Apartment already exists.')

    def create(self):
        model = models.Apartment()
        for key in list(self._fields):
            setattr(model, key, getattr(self, key).data)
        db.session.add(model)
        db.session.commit()

class ApartmentUpdateForm(Form):
    coords = StringField("Coords")
    floor = IntegerField("Id")
    floors = IntegerField("Id")
    area = IntegerField("Area")
    area_living = IntegerField("Area Living")
    area_kitchen = IntegerField("Area Kitchen")
    address = StringField("Address")
    description = StringField("Description")

    def validate_coords(self, field):
        if ["lat", "lng"]-field.data.keys():
            raise validators.ValidationError('Wrong value for coord.')

    def __init__(self, model, *args, **kwargs):
        self.model = model
        super().__init__(*args, **kwargs)

    def update(self):
        for key in list(self._fields):
            setattr(self.model, key, getattr(self, key).data)
        self.model.status = 1
        db.session.add(self.model)
        db.session.commit()
        print(self.model, "successfully updated.")