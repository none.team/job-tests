from app import application, forms

from flask import request, jsonify, render_template


@application.route("/", methods=["GET"])
def index():
    return render_template("index.html")

@application.route("/v1/avito/items/price", methods=["POST"])
def get_items():
    form = forms.ApartmentsPriceForm()
    if not form.load(request.get_json()) or not form.validate():
        return jsonify(form.errors), 422
    apartments = form.get_apartments()
    for apartment in apartments.iterrows():
        attributes = {
            "id_": getattr(apartment[1], 'id'),
            "price": getattr(apartment[1], 'price'),
            "url": getattr(apartment[1], 'uri_mweb')
        }
        apartment = forms.ApartmentCreateForm()
        if apartment.load(attributes) and apartment.validate():
            apartment.create()
    return jsonify(dict(apartments.price.describe()))