import requests, bs4, json, time, re

from app import application, models, Config, forms


@application.cli.command("apatments-load")
def apatments_load():
    for apatment in models.Apartment.query.filter_by(status=0).all():
        request = requests.get(Config.HOST+apatment.url)
        soup = bs4.BeautifulSoup(request.content, 'html.parser')
        for script in filter(lambda s: "window.__initialData__" in s.text, soup.select("script")):
            data = json.loads(script.text.split("window.__initialData__ = ")[1].split(" || {};")[0])
            attributes = {
                "coords": data.get("item", {}).get("item", {}).get("coords"),
                "floor": data.get("item", {}).get("item", {}).get("firebaseParams", {}).get("floor"),
                "floors": data.get("item", {}).get("item", {}).get("firebaseParams", {}).get("floors_count"),
                "area": data.get("item", {}).get("item", {}).get("firebaseParams", {}).get("area"),
                "area_living": data.get("item", {}).get("item", {}).get("firebaseParams", {}).get("area_live"),
                "area_kitchen": data.get("item", {}).get("item", {}).get("firebaseParams", {}).get("area_kitchen"),
                "address": data.get("item", {}).get("item", {}).get("address"),
                "description": data.get("item", {}).get("item", {}).get("description")
            }
            for key in ["floor", "floors", "area", "area_living", "area_kitchen"]:
                attributes[key] = int(re.sub(r"\D", "", attributes[key])) if attributes[key] else 0
            model = forms.ApartmentUpdateForm(apatment)
            if model.load(attributes) and model.validate():
                model.update()
            else:
                print("Apartment not updated, reason: ", model.errors)
        time.sleep(5)