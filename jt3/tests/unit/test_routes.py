
def test_form(client, json):
    rv = client.post("/form",
                     json=json)
    assert rv.status_code == 200
    assert isinstance(rv.json, float)
