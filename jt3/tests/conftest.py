import pytest

from app import app


@pytest.fixture
def client():
    """ Эмулирование приложения.
    """
    client = app.test_client()
    with app.app_context():
        yield client


@pytest.fixture
def json():
    """ Эмитация данных с датчиков.
    """
    return dict(noise=123.0,
                back_edc=123.0,
                mean_voltage_gain=123.0,
                sum_apg=123.0,
                board_temp=[123.0] * 10,
                board_temp_f=[123.0] * 2,
                board_temp_d=[123.0] * 2)

