import numpy as np
import pandas as pd
from random import randint
from flask.cli import AppGroup


from app import app


def gradient(df_, std, mean):
    """ Градиентный спуск, поиск коэфициентов весов.
    """
    X = df_.drop(["product_percents", "Date"], axis=True)
    y = df_.product_percents
    X = ((X-X.mean())/X.std()).values
    y = ((y-y.mean())/y.std()).values

    epoch = 60000
    step = 100
    w = [0] * X.shape[1]
    for e in range(epoch):
        s = X.shape[0]
        i = randint(0, s-1)
        w -= (step/(s+10*e) * (X[i].dot(w)-y[i])) * (X[i])
    return w, X.dot(w) * std + mean


dataset = AppGroup("dataset")

@dataset.command("calc-weights")
def calc_weights():
    """ Адаптация датасета и сохранение весов.
    """
    df = pd.read_csv("data/3min_series.csv")
    df_y = pd.read_csv("data/target.csv", sep=";")
    df.Date = pd.to_datetime(df.Date)
    df_y.loc[:, "Date"] = pd.to_datetime(df_y["Unnamed: 0"])
    df_y.drop("Unnamed: 0", axis=True, inplace=True)
    df = df.set_index(df.Date.apply(lambda x: x.strftime("%Y-%m-%d-%p")))
    df_y = df_y.set_index(df_y.Date.apply(lambda x: x.strftime("%Y-%m-%d-%p")))
    df.loc[:, "product_percents"] = df_y.product_percents
    flt = ~df["Board Temp. 1"].isnull() & ~df["Шум"].isnull() & ~df["Ср.прир.напр."].isnull()
    X_train = df.loc[flt & (df.Date < "2018-10-18") & ~df.product_percents.isnull()]
    X_test = df.loc[flt & (df.Date > "2018-10-18")]
    print("Test shape: {}, train shape: {}".format(X_test.shape[0], X_train.shape[0]))
    board_columns = ["Board Temp. 1",
                     "Board Temp. 2",
                     "Board Temp. 3",
                     "Board Temp. 4",
                     "Board Temp. 5",
                     "Board Temp. 6",
                     "Board Temp. 7",
                     "Board Temp. 8",
                     "Board Temp. 9",
                     "Board Temp. 10"]
    board_columns_f = ["Board Face Temp. 1",
                       "Board Face Temp. 6"]
    board_columns_d = ["Board Deaf Temp. 1",
                       "Board Deaf Temp. 6"]
    w, X = gradient(X_train, X_train.product_percents.std(), X_train.product_percents.mean())
    errors = X - X_train.product_percents.values
    print("Mean error:", sum((errors)**2)/errors.shape[0])
    np.savetxt("weights.csv", w)

app.cli.add_command(dataset)
