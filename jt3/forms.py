import csv
import numpy as np
from wtforms import Form
from wtforms import validators
from wtforms import StringField, IntegerField, FloatField, FieldList, Field, BooleanField


def validate_float(form, field):
    if not field.data is None and not isinstance(field.data, float):
        raise validators.ValidationError("Unexpected value: {}.".format(field.data))


class ObservationForm(Form):
    """ Класс валидации данных полученных с датчиков.

        В случае успешной валидации возвращает продукт X(i)*W
    """

    noise = IntegerField(None, [
        validators.DataRequired(),
        validate_float
    ])

    back_edc = IntegerField(None, [
        validators.DataRequired(),
        validate_float
    ])

    mean_voltage_gain = IntegerField(None, [
        validators.DataRequired(),
        validate_float
    ])

    sum_apg = IntegerField(None, [
        validators.DataRequired(),
        validate_float
    ])

    board_temp = FieldList(IntegerField(None, [validators.DataRequired(),
                                               validate_float]),
                           min_entries=10,
                           max_entries=10)

    board_temp_f = FieldList(IntegerField(None, [validators.DataRequired(),
                                                 validate_float]),
                             min_entries=2,
                             max_entries=2)

    board_temp_d = FieldList(IntegerField(None, [validators.DataRequired(),
                                                 validate_float]),
                             min_entries=2,
                             max_entries=2)

    def get_value(self):
        """ Перемножает веса регресси на данные с датчиков.
            Возвращает калькуляцию либо ошибки валидации.
        """
        if self.validate():
            try:
                with open('weights.csv') as file:
                    w = [ float(line) for line in file.read().split("\n")[:-1] ]
                    w = np.array(w)
                    X = [self.noise.data,
                         self.back_edc.data,
                         self.mean_voltage_gain.data,
                         self.sum_apg.data]
                    for t in self.board_temp.data:
                        X.append(t)
                    for t in self.board_temp_f.data:
                        X.append(t)
                    for t in self.board_temp_d.data:
                        X.append(t)
                    return np.array(X).T.dot(w)
            except IOError as e:
                return {"file": "Weights not found, try to run 'flask dataset calc-weights'"}
        return self.errors