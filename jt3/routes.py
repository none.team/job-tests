from flask import request, jsonify

from app import app
from forms import ObservationForm


@app.route("/form", methods=["POST"])
def form():
    result = ObservationForm(data=request.json)
    return jsonify(result.get_value()), 200